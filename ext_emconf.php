<?php

$EM_CONF[$_EXTKEY] = array (
	'title' => 'FSC migration after import',
	'description' => 'Automatically migrate all imported content elements from css_styled_content to fluid_styled_content.',
	'category' => 'be',
	'version' => '1.0.1',
	'state' => 'stable',
	'uploadfolder' => 0,
	'author' => 'Markus Klein',
	'author_company' => 'Reelworx GmbH',
	'constraints' => array (
		'depends' => array (
			'typo3' => '7.6.0-7.99.99',
            'impexp' => '7.6.0-7.99.99',
            'fluid_styled_content' => '7.6.0-7.99.99',
		),
		'conflicts' => array (
		),
		'suggests' => array (
		),
	),
);
