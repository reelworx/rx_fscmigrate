<?php
defined('TYPO3_MODE') || die('Access denied.');

if (TYPO3_MODE === 'BE') {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/impexp/class.tx_impexp.php']['after_writeRecordsRecords'][] = \Reelworx\RxFscmigrate\Hooks\FscMigration::class . '->migrateText';
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/impexp/class.tx_impexp.php']['after_setRelations'][] = \Reelworx\RxFscmigrate\Hooks\FscMigration::class . '->migrateImages';
}
