<?php
namespace Reelworx\RxFscmigrate\Hooks;

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\DataHandling\DataHandler;

class FscMigration
{
    /**
     * @param array $params
     * @return void
     */
    public function migrateImages(array $params)
    {
        /** @var DatabaseConnection $databaseConnection */
        $databaseConnection = $GLOBALS['TYPO3_DB'];
        /** @var DataHandler $tce */
        $dataHandler = $params['tce'];

        $keysToMigrate = [];
        foreach ($dataHandler->datamap['tt_content'] as $key => $record) {
            if (isset($record['image'])) {
                $keysToMigrate[] = $key;
            }
        }

        if (empty($keysToMigrate)) {
            return;
        }

        // Update 'textpic' and 'image' records
        $query = '
            UPDATE tt_content
            LEFT JOIN sys_file_reference
            ON sys_file_reference.uid_foreign=tt_content.uid
            AND sys_file_reference.tablenames=' . $databaseConnection->fullQuoteStr('tt_content', 'sys_file_reference')
                 . ' AND sys_file_reference.fieldname=' . $databaseConnection->fullQuoteStr('image', 'sys_file_reference')
                 . ' SET tt_content.CType=' . $databaseConnection->fullQuoteStr('textmedia', 'tt_content')
                 . ', tt_content.assets=tt_content.image,
            tt_content.image=0,
            sys_file_reference.fieldname=' . $databaseConnection->fullQuoteStr('assets', 'tt_content')
                 . ' WHERE
            tt_content.CType=' . $databaseConnection->fullQuoteStr('textpic', 'tt_content')
                 . ' OR tt_content.CType=' . $databaseConnection->fullQuoteStr('image', 'tt_content')
            . ' AND tt_content.uid IN (' . implode(',', $keysToMigrate) .  ')';
        $databaseConnection->sql_query($query);
    }

    /**
     * @param array $params
     * @return void
     */
    public function migrateText(array $params)
    {
        /** @var DatabaseConnection $databaseConnection */
        $databaseConnection = $GLOBALS['TYPO3_DB'];
        /** @var DataHandler $tce */
        $dataHandler = $params['tce'];

        if (empty($dataHandler->datamap['tt_content'])) {
            return;
        }

        $keysToMigrate = [];
        foreach ($dataHandler->datamap['tt_content'] as $key => $record) {
            if (isset($record['CType']) && $record['CType'] === 'text') {
                if (isset($dataHandler->substNEWwithIDs[$key])) {
                    $keysToMigrate[] = $dataHandler->substNEWwithIDs[$key];
                } else {
                    $keysToMigrate[] = $key;
                }
            }
        }

        if (empty($keysToMigrate)) {
            return;
        }

        // Update 'text' records
        $databaseConnection->exec_UPDATEquery(
            'tt_content',
            'tt_content.uid IN (' . implode(',', $keysToMigrate) . ')',
            [
                'CType' => 'textmedia',
            ]
        );
    }
}
