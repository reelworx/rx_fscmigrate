Automated Fluid Styled Content migration on import
==================================================

TYPO3 extension which automatically migrates imported records (by ext:impexp) from css_styled_content to fluid_styled_content.

Created by Reelworx GmbH http://reelworx.at/
License: GPL 3+
